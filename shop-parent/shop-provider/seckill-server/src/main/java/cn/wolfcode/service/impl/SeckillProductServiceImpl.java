package cn.wolfcode.service.impl;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.Product;
import cn.wolfcode.domain.SeckillProduct;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.feign.ProductFeignApi;
import cn.wolfcode.mapper.SeckillProductMapper;
import cn.wolfcode.redis.SeckillRedisKey;
import cn.wolfcode.service.ISeckillProductService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
public class SeckillProductServiceImpl implements ISeckillProductService {

    @Autowired
    private SeckillProductMapper seckillProductMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RocketMQTemplate rocketMQTemplate;
    @Autowired
    private ProductFeignApi productFeignApi;

    @Override
    public List<SeckillProductVo> selectByTime(int time) {
        //1.查询秒杀商品的集合数据
        List<SeckillProduct> seckillProducts = seckillProductMapper.queryCurrentlySeckillProduct(time);
        //2.把商品id 放到list集合中
        List<Long> ids = new ArrayList<>();
        for (SeckillProduct seckillProduct : seckillProducts) {
            ids.add(seckillProduct.getProductId());
        }

        //3.远程去调用我们的商品服务,将商品查询出来
        Result<List<Product>> result = productFeignApi.queryByIds(ids);
        List<SeckillProductVo> vos = new ArrayList<>();
        if (result != null && !result.hasError()){
            //4.进行集合操作,最终返回List<SeckillProductVO>
            List<Product> products = result.getData();
            Map<Long,Product> map = new HashMap<>();
            for (Product product : products) {
                map.put(product.getId(),product);
            }
            for (SeckillProduct seckillProduct : seckillProducts) {
                SeckillProductVo seckillProductVo = new SeckillProductVo();
                Product product = map.get(seckillProduct.getProductId());
                BeanUtils.copyProperties(product,seckillProductVo);
                BeanUtils.copyProperties(seckillProduct,seckillProductVo);
                vos.add(seckillProductVo);
            }
        }
        return vos;
    }

    @Override
    public List<SeckillProductVo> queryByTime(int time) {
        List<Object> list = redisTemplate.opsForHash().values(SeckillRedisKey.SECKILL_PRODUCT_LIST.getRealKey(time + ""));
        List<SeckillProductVo> vos = new ArrayList<>();
        for (Object objJson : list) {
            vos.add(JSON.parseObject(objJson+"",SeckillProductVo.class));
        }
        return vos;
    }

    @Override
    public SeckillProductVo find(int time, Long seckillId) {
        Object objJson = redisTemplate.opsForHash().get(SeckillRedisKey.SECKILL_PRODUCT_LIST.getRealKey(time + ""), seckillId+"");
        if (StringUtils.isEmpty(objJson+"")){
            return null;
        }
        return JSON.parseObject(objJson+"",SeckillProductVo.class);
    }

    @Override
    public void synRedisStockCount(Integer time, Long seckillId) {
        int stockCount = seckillProductMapper.getStockCount(seckillId);
        redisTemplate.opsForHash().put(SeckillRedisKey.SECKILL_STOCK_COUNT_HASH.getRealKey(time+""),seckillId+"",stockCount);
        log.info("库存同步成功...");
    }
}
