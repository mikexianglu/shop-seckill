package cn.wolfcode.mq;

import cn.wolfcode.service.ISeckillProductService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * ClassName MQOrderFailListener
 * Create by Mike
 * Date 2021/10/25 21:01
 */
@Component
@RocketMQMessageListener(
    consumerGroup = "MQOrderFailListener",
        topic = MQConstant.ORDER_RESULT_TOPIC,
        selectorExpression = MQConstant.ORDER_RESULT_FAIL_TAG,
        selectorType = SelectorType.TAG
)
@Slf4j
public class MQOrderFailListener implements RocketMQListener<OrderMQResult> {

    @Autowired
    private ISeckillProductService seckillProductService;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Override
    public void onMessage(OrderMQResult orderMQResult) {
        log.info("进入到失败的消费者中...");
        //1.回补预库存
        seckillProductService.synRedisStockCount(orderMQResult.getTime(),orderMQResult.getSeckillId());
        //2.修改本地标识
        rocketMQTemplate.sendOneWay(MQConstant.CANCEL_SECKILL_OVER_SIGE_TOPIC,orderMQResult.getSeckillId());
    }
}
