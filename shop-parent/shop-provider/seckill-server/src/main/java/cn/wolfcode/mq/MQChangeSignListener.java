package cn.wolfcode.mq;

import cn.wolfcode.web.controller.OrderInfoController;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * ClassName MQChangeSignListener
 * Create by Mike
 * Date 2021/10/25 21:09
 */
@Component
@RocketMQMessageListener(
    consumerGroup = "MQChangeSignListener",
        topic = MQConstant.CANCEL_SECKILL_OVER_SIGE_TOPIC,
        messageModel = MessageModel.BROADCASTING
)
@Slf4j
public class MQChangeSignListener implements RocketMQListener<Long> {

    @Override
    public void onMessage(Long seckillId) {
        log.info("进入到修改本地标识消费者中...");
        OrderInfoController.local_flag.put(seckillId,false);
    }
}
