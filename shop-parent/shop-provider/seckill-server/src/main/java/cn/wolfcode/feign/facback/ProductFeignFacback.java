package cn.wolfcode.feign.facback;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.Product;
import cn.wolfcode.feign.ProductFeignApi;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ClassName ProductFeignFacback
 * Create by Mike
 * Date 2021/10/23 18:55
 */
@Component
public class ProductFeignFacback implements ProductFeignApi {

    @Override
    public Result<List<Product>> queryByIds(List<Long> ids) {
        return null;
    }
}
