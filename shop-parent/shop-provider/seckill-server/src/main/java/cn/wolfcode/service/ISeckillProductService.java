package cn.wolfcode.service;

import cn.wolfcode.domain.SeckillProduct;
import cn.wolfcode.domain.SeckillProductVo;

import java.util.List;


public interface ISeckillProductService {

    /**
     * 根据场次把秒杀列表数据查询出来
     * @param time 场次
     * @return
     */
    List<SeckillProductVo> selectByTime(int time);

    /**
     * 前台界面展示的秒杀列表数据
     * @param time
     * @return
     */
    List<SeckillProductVo> queryByTime(int time);

    /**
     * 返回商品详情
     * @param time 场合数
     * @param seckillId 商品id
     * @return
     */
    SeckillProductVo find(int time, Long seckillId);

    /**
     * 回补预库存
     * @param time
     * @param seckillId
     */
    void synRedisStockCount(Integer time, Long seckillId);
}
