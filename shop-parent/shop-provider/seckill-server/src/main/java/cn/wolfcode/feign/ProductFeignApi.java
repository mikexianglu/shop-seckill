package cn.wolfcode.feign;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.Product;
import cn.wolfcode.feign.facback.ProductFeignFacback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * ClassName ProductFeignApi
 * Create by Mike
 * Date 2021/10/23 18:53
 */
@FeignClient(name = "product-service",fallback = ProductFeignFacback.class)
public interface ProductFeignApi {

    @RequestMapping("/product/queryByIds")
    Result<List<Product>> queryByIds(@RequestParam("ids") List<Long> ids);
}
