package cn.wolfcode.feign;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.feign.facback.SeckillProductFeignFacback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * ClassName SeckillProductFeignApi
 * Create by Mike
 * Date 2021/10/23 18:31
 */
@FeignClient(name = "seckill-service",fallback = SeckillProductFeignFacback.class)
public interface SeckillProductFeignApi {

    @RequestMapping("/seckillProduct/selectByTime")
    Result<List<SeckillProductVo>> queryByTime(@RequestParam("time") int time);
}
