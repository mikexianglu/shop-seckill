package cn.wolfcode.feign.facback;

import cn.wolfcode.common.web.Result;
import cn.wolfcode.domain.SeckillProductVo;
import cn.wolfcode.feign.SeckillProductFeignApi;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ClassName SeckillProductFeignFacback
 * Create by Mike
 * Date 2021/10/23 18:35
 */
@Component
public class SeckillProductFeignFacback implements SeckillProductFeignApi {

    @Override
    public Result<List<SeckillProductVo>> queryByTime(int time) {
        return null;
    }
}
